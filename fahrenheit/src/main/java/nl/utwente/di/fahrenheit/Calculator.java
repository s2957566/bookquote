package nl.utwente.di.fahrenheit;

public class Calculator {
    double getFahrenheit(double celsius) {
        return (celsius * 9/5) + 32;
    }
}
