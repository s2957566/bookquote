package nl.utwente.di.fahrenheit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests the Quoter
 */
public class TestCalculator {
    @Test
    public void testBook1 ( ) throws Exception {
        Calculator quoter = new Calculator() ;
        double celc = quoter.getFahrenheit(10);
        Assertions.assertEquals(50.0, celc, 0.0, "Fahrenheit of 10 celcius.");
    }
}
