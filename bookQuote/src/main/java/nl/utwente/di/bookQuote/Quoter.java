package nl.utwente.di.bookQuote;

public class Quoter {
    double getBookPrice(String isbn) {
        return switch (isbn) {
            case "1" -> 10;
            case "2" -> 45;
            case "3" -> 20;
            case "4" -> 35;
            case "5" -> 50;
            default -> 0;
        };
    }
}
